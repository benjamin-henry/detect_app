import pyinotify
import sys
import threading
import paho.mqtt.client as mqtt
from json import loads
from time import sleep

#lib de commandes
import win32com.client
from pywinauto import application

#params
config_path = "command_config.json"

IP_MQTTServer = '192.168.1.36'
PORT_MQTTServer = 1883
timeout_mqtt = 60

run = True

app = application.Application.start("notepad.exe")
try:
	command_config = loads(open(config_path,'r').read())
except (JSONDecodeError, IOError) as e:
	print("Config file command_config.json does not exist or is corrupted, exception : " + str(e))
	# command par defaut
	command_config = {
		"top": app.notepad.TypeKeys("%FT"),		#position static haute
		"bot": app.notepad.TypeKeys("%FB"),		#position static basse...
		"rig": app.notepad.TypeKeys("%FR"),
		"lef": app.notepad.TypeKeys("%FL"),
		"turn_l": app.notepad.TypeKeys("%FTL"),		#tour anti-horaire
		"turn_r": app.notepad.TypeKeys("%FTR")		#tour horaire
	}
	pass

#MQTT
def on_connect(clent, userdata, flags, rc):
    for topic in topics_sub.keys():
	    client.subscribe(topic)

def on_message(client, userdata, msg):
	switcher(msg.topic,msg.payload)

#Switcher
def switcher(topic,msg):
	function = topics_sub.get(topic)
	return function(msg)

#pour le moment commandes simple
def execute(msg):
	command_config[msg]

def mode(state): #publie une mise à jour sur l'état du système
	client.publish(topics_pub["mode"],mode_states[state])

mode_states = {
	"sleep": 0,
	"wake": 1
}
	
# MQTT

def mqtt_loop():
	while run:
		client.loop()

#Topics
topics_sub = {
	"/module/command": execute
}

topics_pub = {
	"mode": "/module/mode"
}

try :
	#Setup Client
	client = mqtt.Client()
	client.on_connect = on_connect
	client.on_message = on_message

	client.connect(IP_MQTTServer, PORT_MQTTServer, timeout_mqtt)

	#lancement du thread notifier
	thread_mqtt = threading.Thread(target = mqtt_loop, args = ())
	thread_mqtt.daemon = True
	thread_mqtt.start()
	
	app = application.Application.start("notepad.exe")
	
	while run:
		sleep(1)

except:
	raise
finally:
	run = False
	thread_mqtt.join()